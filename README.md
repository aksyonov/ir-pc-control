ir-pc-control
==============

Simple project to control PC using any IR remote. Made for my purposes with SAMSUNG remote control.

![scheme](https://bitbucket.org/aksyonov/ir-pc-control/raw/master/sketch.png)

## Components

- Arduino Pro Micro
- TSOP4838

## Notes

To use with another remote or add more commands, simply edit switch statement inside `ir-pc-control.ino`.
