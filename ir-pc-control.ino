#include <HID-Project.h>
#include <IRremote.h>

const int RECV_PIN = 9;
const int RXLED = 30;
const int TXLED = 17;

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(9600);
  // Disable red led
  pinMode(RXLED, INPUT);
  pinMode(TXLED, INPUT);

  // Initialize pins for IR receiver VCC and GND
  pinMode(15, OUTPUT);
  pinMode(10, OUTPUT);
  digitalWrite(15, LOW);
  digitalWrite(10, HIGH);

  Serial.println("Enabling IRin");
  irrecv.enableIRIn(); // Start the receiver
  Serial.println("Enabled IRin");

  Consumer.begin();
  Keyboard.begin();
}

void loop() {
  if (irrecv.decode(&results)) {
    switch (results.value) {

      // Media keys
      case 0x8020E2F0:
      case 0xDB779EA1:
        Consumer.write(MEDIA_PLAY_PAUSE);
        break;
      case 0x8020D0F8:
      case 0x28E869FD:
        Consumer.write(MEDIA_STOP);
        break;
      case 0x80200ED0:
        Consumer.write(MEDIA_FAST_FORWARD);
        break;
      case 0x80204630:
        Consumer.write(MEDIA_REWIND);
        break;
      case 0x8020C810:
      case 0xA0E9FC95:
        Consumer.write(MEDIA_NEXT);
        break;
      case 0x8020D808:
      case 0xE5ED1B19:
        Consumer.write(MEDIA_PREVIOUS);
        break;

      // Keyboard
      case 0x8020A090:
      case 0x553A1357:
        Keyboard.write(KEY_ENTER);
        break;
      case 0x8020B008:
      case 0x032FFE31:
        Keyboard.write(KEY_UP_ARROW);
        break;
      case 0x8020A850:
      case 0xC9F3567D:
        Keyboard.write(KEY_DOWN_ARROW);
        break;
      case 0x8020A4D0:
      case 0x9DC14B71:
        Keyboard.write(KEY_LEFT_ARROW);
        break;
      case 0x8020B808:
      case 0xCA017DFD:
        Keyboard.write(KEY_RIGHT_ARROW);
        break;
      case 0x80204EB0:
      case 0x70049F63:
        Keyboard.write(KEY_ESC);
        break;
      case 0x80206CB0:
        Keyboard.write(KEY_LEFT_GUI);
        break;
      case 0x8020F488:
        Keyboard.write('0');
        break;
      case 0x80208290:
        Keyboard.write('1');
        break;
      case 0x80204250:
        Keyboard.write('2');
        break;
      case 0x8020C2D0:
        Keyboard.write('3');
        break;
      case 0x80202230:
        Keyboard.write('4');
        break;
      case 0x8020A2B0:
        Keyboard.write('5');
        break;
      case 0x80206270:
        Keyboard.write('6');
        break;
      case 0x80203E68:
        Keyboard.write('7');
        break;
      case 0x8020FC48:
        Keyboard.write('8');
        break;
      case 0x8020EC70:
        Keyboard.write('9');
        break;
      case 0x8020E4B0:
        Keyboard.write('h');
        Keyboard.write('e');
        Keyboard.write('l');
        Keyboard.write('l');
        Keyboard.write('o');
        break;
      default:
        Serial.println(results.value, HEX);
    }
    irrecv.resume(); // Receive the next value
  }
  delay(100);
}
